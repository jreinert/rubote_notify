#!/usr/bin/env ruby

SERVER_URI = "druby://example.com:9090"
PASSWORD = "change me"

pipe = IO.popen("-")

if pipe
  puts pipe.gets
  puts "PID: #{pipe.pid}"
  exit
end

require "drb"
require "shellwords"

@server = DRbObject.new_with_uri(SERVER_URI)

def format_message message
  buffer = message[:buffer]
  prefix = message[:prefix]
  message = message[:message]
  "<i>#{buffer || "Query"}:</i><br><b>#{prefix}</b> | #{message}"
end

puts "Listening for notifications from #{SERVER_URI}"
loop do
  message = @server.listen(PASSWORD)
  next if message == :heartbeat
  formatted_message = format_message(message)
  `notify-send #{Shellwords.escape(formatted_message)}`
end
