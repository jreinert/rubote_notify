#!/usr/bin/env ruby

require 'drb'
require 'digest'

class NotificationServer

  def initialize password
    @notification_queue = Queue.new
    @password = password
    Thread.new do
      loop do
        push(:heartbeat)
        sleep 30
      end
    end
  end

  def listen password
    authenticate password
    @notification_queue.pop
  end

  def push(password, notification)
    authenticate password
    @notification_queue.push(notification)
  end

  private

  def authenticate(password)
    sleep 1 # slow down brute forcing
    raise AuthError unless password == @password
  end

  class AuthError < SecurityError; end

end

port = ARGV[0] || exit(1)
password = STDIN.gets.chomp
pipe = IO.popen('-')
if pipe
  puts pipe.gets
  puts "PID: #{pipe.pid}"
else
  $PROGRAM_NAME = "Notification server"
  $SAFE = 1
  DRb.start_service("druby://:#{port}", NotificationServer.new(password))
  puts "Started notification server on #{DRb.uri}"
  trap('HUP') { DRb.stop_service }
  trap('TERM') { DRb.stop_service }
  trap('INT') { DRb.stop_service }
  DRb.thread.join
end
